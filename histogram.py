from collections import Counter
filename = input("What is the name of your file?")
data = open(filename).read()  # read the file
data = ''.join( [i.upper() if i.isalpha() else ' ' for i in data] )   # remove the punctuation
c = Counter( data.split() )   # count the words

values_list = c.values()
word_sum = 0

for v in values_list:
    word_sum += v # get the number of words in the file

percent_dict = {}
for k, v in c.items():
    percentage = (100*v)/word_sum
    percent_dict[k] = percentage

    print(percent_dict)